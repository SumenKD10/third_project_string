function string3(stringPassed) {
    if (stringPassed === undefined || stringPassed.length === 0) {
        return [];
    }
    let month = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'Decemeber'];
    let result;
    stringPassed = stringPassed.split('/');
    result = month[Number(stringPassed[1]) - 1];
    return result;
}

module.exports = string3;