function string2(stringPassed) {
    if (stringPassed === undefined || stringPassed.length === 0) {
        return [];
    }
    let reg = /^((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9][0-9]|[0-9])\.){3}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9][0-9]|[0-9])$/ig;
    if (!reg.test(stringPassed)) {
        return [];
    } else { 
        let result;
        result = stringPassed.split('.');
        result = result.map(variable => Number(variable));
        return result;
    }
}

module.exports = string2;