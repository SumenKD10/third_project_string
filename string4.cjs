function string4(objectName) {
    let result=[];
    for(let property in objectName){
        result.push(objectName[property].slice(0,1).toUpperCase()+objectName[property].slice(1).toLowerCase());
    }
    return result.join(" ");

}

module.exports = string4;