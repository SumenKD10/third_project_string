function string1(stringPassed) {
    let answer = [];
    if (stringPassed === undefined || stringPassed.length === 0) {
        return 0;
    }
    let reg = /([$,])/g;
    stringPassed = stringPassed.replaceAll(reg, '');
    if (isNaN(stringPassed) === true) {
        return 0;
    } else {
        answer.push(Number(stringPassed));
    }
    return answer;
}

module.exports = string1;