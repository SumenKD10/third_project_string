function string5(stringArray) {
    if(stringArray.length === 0 || Array.isArray(stringArray) != true){
        return "";
    } else {
        return stringArray.join(" ")+".";
    }
}

module.exports = string5;